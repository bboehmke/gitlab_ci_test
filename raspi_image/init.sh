#!/sbin/openrc-run

command="/usr/bin/test_server"
pidfile="/var/run/test_server.pid"
command_args=""
command_background=true


depend() {
	use logger dns
	need net
	after firewall
}
