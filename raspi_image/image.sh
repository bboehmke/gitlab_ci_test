#!/bin/sh

cp ${INPUT_PATH}/out/test_server_linux_arm ${ROOTFS_PATH}/usr/bin/test_server
cp ${INPUT_PATH}/raspi_image/init.sh ${ROOTFS_PATH}/etc/init.d/test_server

chmod +x ${ROOTFS_PATH}/usr/bin/test_server
chmod +x ${ROOTFS_PATH}/etc/init.d/test_server

chroot_exec rc-update add test_server default
